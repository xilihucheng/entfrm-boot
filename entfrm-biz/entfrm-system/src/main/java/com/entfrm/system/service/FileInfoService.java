package com.entfrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.system.entity.FileInfo;

/**
 * @author entfrm
 * @date 2019-09-30 14:17:03
 *
 * @description 文件Service接口
 */
public interface FileInfoService extends IService<FileInfo> {

}
