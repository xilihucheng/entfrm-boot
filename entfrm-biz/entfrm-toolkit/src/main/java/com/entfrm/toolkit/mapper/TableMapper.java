package com.entfrm.toolkit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.toolkit.entity.Table;


/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface TableMapper extends BaseMapper<Table> {

}
