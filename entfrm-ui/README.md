## 开发

```bash
# 克隆项目
git clone https://gitee.com/entfrm/entfrm-boot.git

# 进入项目目录
cd h5ve-ui

# 安装依赖
npm install

yarn install

# 淘宝镜像
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev

yarn run dev

```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```