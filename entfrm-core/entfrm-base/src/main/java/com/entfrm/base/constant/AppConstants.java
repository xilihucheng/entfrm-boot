package com.entfrm.base.constant;

/**
 * @author entfrm
 * @date 2022-02-15
 * @description 应用前缀
 */
public interface AppConstants {

    String APP_SYSTEM = "/system";

    String APP_TOOLKIT = "/toolkit";

    String APP_MONITOR = "/monitor";

}
